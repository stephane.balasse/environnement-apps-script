const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;

const {entries} = require("./config/entrys");
const htmlWebpackPluginArr = require('./config/createHtmlWebpackPlugin');
const InlineChunkHtmlPlugin = require('inline-chunk-html-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");


module.exports = {
    mode: "production",
    entry: entries(),
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {},
                    },
                    "css-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: 'dist/[name].js',
        path: path.resolve(__dirname, ''),
        chunkFilename: 'static/js/[id].chunk.js',
        sourceMapFilename: '[name].map',
        library: 'code'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        ...htmlWebpackPluginArr,
        new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/.(js)$/]),

        new HTMLInlineCSSWebpackPlugin()
    ],
};
