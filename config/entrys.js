const glob = require('glob')
const path = require('path')

const PAGES_DIR = path.resolve(__dirname, '../client')

exports.entries = function () {
    const entryFiles = glob.sync(PAGES_DIR + '/!(cores)*/*.ts')
    const resultEntry = {}
    entryFiles.forEach((filePath) => {
        const filename = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.'))
        resultEntry[filename] = filePath
    })
    return resultEntry
}

exports.htmlPages = function () {
    const entryHtmls = glob.sync(PAGES_DIR + '/*/*.html')
    const resultHtmlPages = []

    entryHtmls.forEach((filePath) => {
        const filename = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.'))

        const htmlPlugin = {
            template: filePath,
            filename: filename.replace(/^\w/, (c) => c.toUpperCase()) + '.html',
            chunks: filename,
            inject: true,
            inlineSource: '.(js|css)$'
        }

        resultHtmlPages.push(htmlPlugin)
    })

    return resultHtmlPages
}
